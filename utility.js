export function focusFirstEle(component,element) {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, lightning-helptext, lightning-input, lightning-button';
        const modal = component.template.querySelector('.slds-modal');
        
        const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
        const focusableContent = modal.querySelectorAll(focusableElements);
        const lastFocusableElement = focusableContent[focusableContent.length - 1];
        
        if(element) {
            const focusableElement = modal.querySelectorAll(focusableElements)[element];
            focusableElement.focus();
        }
        else {
            firstFocusableElement.focus();
        }
        component.template.addEventListener('keydown', function(event) {
            let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
            if (!isTabPressed) {
                return;
            }
            if (event.shiftKey) {       
				if (component.template.activeElement === firstFocusableElement) {
                    lastFocusableElement.focus(); 
                    event.stopPropagation()
                    event.preventDefault();
                }
            } else { 
                if (component.template.activeElement === lastFocusableElement) {  
                    firstFocusableElement.focus(); 
                    event.preventDefault();
                    event.stopPropagation()
                }
            }
        });
}