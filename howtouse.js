import { focusFirstEle } from "c/util";

export class something extends LightningElement{

    renderedCallback() {
	 	focusFirstEle(this);
	 }
     
     //use this when focus should go to particular element inside the modal
     renderedCallback() {
         focusFirstEle(this,2);
     }
}